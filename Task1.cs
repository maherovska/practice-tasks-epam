using System;

public class Vector
{
	private int _x;
	private int _y;
	private int _z;
	
	public Vector()
		:this(0, 0, 0)
	{	
	}
	
	public Vector(int x, int y, int z)
	{
		this._x = x;
		this._y = y;
		this._z = z;
	}
	
	public static Vector operator+ (Vector vector1, Vector vector2)
	{		
		Vector result = new Vector();
		result._x = vector1._x + vector2._x;
		result._y = vector1._y + vector2._y;
		result._z = vector1._z + vector2._z;
		
		return result;
	}
	
	public static Vector operator- (Vector vector1, Vector vector2)
	{
		Vector result = new Vector();
		result._x = vector1._x - vector2._x;
		result._y = vector1._y - vector2._y;
		result._z = vector1._z - vector2._z;
		
		return result;
	}
	
	public static int ScalarProduct(Vector vector1, Vector vector2)
	{
		return vector1._x * vector2._x + vector1._y * vector2._y + vector1._z * vector2._z;
	}
	
	public static Vector VectorProduct(Vector vector1, Vector vector2)
	{
		Vector result = new Vector();
		result._x = vector1._y * vector2._z - vector1._z * vector2._y;
		result._y = vector1._z * vector2._x - vector1._x * vector2._z;
		result._z = vector1._x * vector2._y - vector1._y * vector2._x;
		
		return result;
	}
	
	public static int MixedProduct(Vector vector1, Vector vector2, Vector vector3)
	{		
		return Vector.ScalarProduct(vector1, Vector.VectorProduct(vector2, vector3));
	}
	
	public static double Norm(Vector vector)
	{
		return Math.Sqrt(Vector.ScalarProduct(vector, vector));
	}
	
	public static double AngleBetweenTwoVectors(Vector vector1, Vector vector2)
	{
		// I use expression "* 180 / Math.PI" to convert from radians to degrees
		return Math.Round(Math.Acos(Vector.ScalarProduct(vector1, vector2) / Vector.Norm(vector1) / Vector.Norm(vector2)) * 180 / Math.PI, 1);
	}
	
	public void Add(Vector vectorToAdd)
	{		
		this._x += vectorToAdd._x;
		this._y += vectorToAdd._y;
		this._z += vectorToAdd._z;
	}
	
	public void Subtract(Vector vectorToSubtract)
	{
		this._x -= vectorToSubtract._x;
		this._y -= vectorToSubtract._y;
		this._z -= vectorToSubtract._z;
	}
	
	public int ScalarProduct(Vector vector)
	{
		return this._x * vector._x + this._y * vector._y + this._z * vector._z;
	}
	
	public void VectorProduct(Vector vector)
	{		
		Vector result = new Vector();
		result._x = this._y * vector._z - this._z * vector._y;
		result._y = this._z * vector._x - this._x * vector._z;
		result._z = this._x * vector._y - this._y * vector._x;		
	
		this._x = result._x;
		this._y = result._y;
		this._z = result._z;
	}
	
	public int MixedProduct(Vector vector2, Vector vector3)
	{		
		vector2.VectorProduct(vector3);
		return this.ScalarProduct(vector2);
	}
	
	public double Norm()
	{
		return Math.Sqrt(this.ScalarProduct(this));
	}
	
	public double AngleBetweenTwoVectors(Vector vector)
	{
		// I use expression "* 180 / Math.PI" to convert from radians to degrees
		return Math.Round(Math.Acos(this.ScalarProduct(vector) / this.Norm() / vector.Norm()) * 180 / Math.PI, 1);
	}
	
	public void InitializeVector(int x, int y, int z)
	{
		this._x = x;
		this._y = y;
		this._z = z;
	}
	
	public override bool Equals(object ob)
	{
		if (ob == null)
		{
			return false;
		}
		
		Vector vectorToCompare = ob as Vector;
		if(vectorToCompare == null)
		{
			return false;
		}
		
		return this._x == vectorToCompare._x
			&& this._y == vectorToCompare._y
			&& this._z == vectorToCompare._z;
	}	
	
	public override int GetHashCode()
	{
		return this._x ^ this._y ^ this._z;
	}
	
	public override string ToString()
	{		
		return String.Format("({0}, {1}, {2})", this._x, this._y, this._z);
	}
}

class Program
{
	static void Main()
	{
		Console.WriteLine("Hello! This is my program #1!\n");
		
		Vector vectorA = new Vector(-1, 1, 5);
		Vector vectorB = new Vector(-3, 3, 4);
		Vector vectorC = new Vector(-3, 3, 4);
		Vector vectorD = new Vector();
		
		Console.WriteLine("I've just created 4 vectors.\n");
		
		Console.WriteLine("vectorA = {0}" , vectorA.ToString());
		Console.WriteLine("vectorB = {0}" , vectorB.ToString());
		Console.WriteLine("vectorC = {0}" , vectorC.ToString());	
		Console.WriteLine("vectorD = {0}" , vectorD.ToString());		
		Console.WriteLine();
		
		Vector resultOfAddition = vectorA + vectorD;
		Console.WriteLine("Addition:");
		Console.WriteLine("{0} + {1} = {2}", vectorA.ToString(), vectorD.ToString(), resultOfAddition.ToString());
		Console.WriteLine();
		
		Vector resultOfSubtraction = vectorB - vectorC;
		Console.WriteLine("Subtraction:");
		Console.WriteLine("{0} - {1} = {2}", vectorB.ToString(), vectorC.ToString(), resultOfSubtraction.ToString());
		Console.WriteLine();
		
		Console.WriteLine("Scalar product A{0} and B{1}:", vectorA.ToString(), vectorB.ToString());		
		Console.WriteLine("AB = {0}", Vector.ScalarProduct(vectorA, vectorB));
		Console.WriteLine();
		
		Console.WriteLine("Vector product A{0} and B{1}:", vectorA.ToString(), vectorB.ToString());
		Console.WriteLine("A*B = {0}", Vector.VectorProduct(vectorA, vectorB).ToString());
		Console.WriteLine();

		Console.WriteLine("Mixed product A{0}, B{1} and C{2}", vectorA.ToString(), vectorB.ToString(), Vector.VectorProduct(vectorA, vectorB).ToString());
		Console.WriteLine("A[B*C] = {0}", Vector.MixedProduct(vectorA, vectorB, Vector.VectorProduct(vectorA, vectorB)));
		Console.WriteLine();
		
		Console.WriteLine("A = {0}", vectorA.ToString());
		Console.WriteLine("|A| = {0}", Vector.Norm(vectorA));
		Console.WriteLine();
		
		Console.Write("Angle between vectorA and vectorB = ");
		Console.WriteLine(Vector.AngleBetweenTwoVectors(vectorA, vectorB));
		Console.WriteLine();
		
		
		Console.WriteLine("\nLet's try that actions with non-static methods!");				
		
		vectorA.Add(vectorD);
		Console.WriteLine("{0} = {0} + {1} \nresult: {2}", vectorA.ToString(), vectorD.ToString(), vectorA.ToString());
		Console.WriteLine();		
		
		vectorB.Subtract(vectorC);
		Console.WriteLine("{0} = {0} - {1} \nresult: {2}", vectorB.ToString(), vectorC.ToString(), vectorB.ToString());
		Console.WriteLine();
		
		vectorA.InitializeVector(-1, 1, 5);
		vectorB.InitializeVector(-3, 3, 4);
		
		Console.WriteLine("Scalar product A{0} and B{1}:", vectorA.ToString(), vectorB.ToString());		
		Console.WriteLine("AB = {0}", vectorA.ScalarProduct(vectorB));
		Console.WriteLine();
		
		vectorA.InitializeVector(-1, 1, 5);
		
		Console.WriteLine("Vector product A{0} and B{1}:", vectorA.ToString(), vectorB.ToString());
		vectorA.VectorProduct(vectorB);
		Console.WriteLine("A*B = {0}", vectorA.ToString());
		Console.WriteLine();
		
		vectorA.InitializeVector(-1, 1, 5);
		
		Console.WriteLine("Mixed product A{0}, B{1} and C{2}", vectorA.ToString(), vectorB.ToString(), Vector.VectorProduct(vectorA, vectorB).ToString());
		Console.WriteLine("A[B*C] = {0}", vectorA.MixedProduct(vectorB, Vector.VectorProduct(vectorA, vectorB)));
		Console.WriteLine();
		
		vectorA.InitializeVector(-1, 1, 5);
		
		Console.WriteLine("A = {0}", vectorA.ToString());
		Console.WriteLine("|A| = {0}", vectorA.Norm());
		Console.WriteLine();
		
		vectorA.InitializeVector(-1, 1, 5);
		vectorB.InitializeVector(-3, 3, 4);
		
		Console.Write("Angle between vectorA and vectorB = ");
		Console.WriteLine(vectorA.AngleBetweenTwoVectors(vectorB));
		Console.WriteLine();
		
		
		Console.WriteLine("A = {0}, B = {1}, C = {2}", vectorA.ToString(), vectorB.ToString(), vectorC.ToString());		
		Console.WriteLine("A == B : {0}", vectorA.Equals(vectorB));
		Console.WriteLine("B == C : {0}", vectorB.Equals(vectorC));
		
	}
}