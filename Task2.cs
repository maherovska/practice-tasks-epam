using System;

public class Rectangle
{	
	// top left point of rectangle
	private double _x;
	private double _y;
	private double _width;
	private double _height;
	
	public Rectangle()
	{
		this._x = 0.0;
		this._y = 0.0;
		this._width = 1.0;
		this._height = 1.0;
	}
	
	public Rectangle(double width, double height)
	{
		this._x = 0.0;
		this._y = 0.0;
		this.Width = width;	
		this.Height = height;
	}
	
	public Rectangle(double x, double y, double width, double height)
	{
		this._x = x;
		this._y = y;
		this.Width = width;	
		this.Height = height;
	}
	
	public double Width
	{
		get
		{
			return this._width;
		}
		set
		{
			if(value > 0.0)
			{
				this._width = value;
			}
			else 
			{
				this._width = 1.0;
			}
		}
	}
	
	public double Height
	{
		get 
		{
			return this._height;
		}
		set
		{
			if(value > 0.0)
			{
				this._height = value;
			}
			else
			{
				this._height = 1.0;
			}
		}
	}
	
	public static void TheLeastRectangle(Rectangle rectangle1, Rectangle rectangle2)
	{		
		Rectangle result = new Rectangle();
		
		result._x = Math.Min(rectangle1._x, rectangle2._x);
		result._y = Math.Min(rectangle1._y, rectangle2._y);
		result._width = Math.Max(rectangle1._x + rectangle1._width, rectangle2._x + rectangle2._width) - result._x;
		result._height = Math.Max(rectangle1._y + rectangle1._height, rectangle2._y + rectangle2._height) - result._y;
		
		Console.WriteLine(result.ToString());
	}	
	
	public static void ResultOfIntersection(Rectangle rectangle1, Rectangle rectangle2)
	{
		Rectangle result = new Rectangle();
		
		result._x = Math.Max(rectangle1._x, rectangle2._x);
		result._y = Math.Max(rectangle1._y, rectangle2._y);
		double bottomRigthX = Math.Min(rectangle1._x + rectangle1._width, rectangle2._x + rectangle2._width);
		double bottomRigthY = Math.Min(rectangle1._y + rectangle1._height, rectangle2._y + rectangle2._height);
		
		if(result._x < bottomRigthX && result._y < bottomRigthY)
		{
			result._width = bottomRigthX - result._x;
			result._height = bottomRigthY - result._y;
			Console.WriteLine(result.ToString());
		}
		else
		{
			Console.WriteLine("These rectangles don't intersect!");
		}
	}
	
	public void MoveRectangle(double x, double y)
	{
		this._x += x;
		this._y += y;
	}
	
	public void ChangeLocation(double x, double y)
	{
		this._x = x;
		this._y = y;
	}
	
	public void ZoomSize(double zoom)
	{
		this.ZoomWidth(zoom);
		this.ZoomHeight(zoom);
	}
	
	public void ZoomWidth(double zoom)
	{
		if(zoom > 0.0)
		{
			this.Width *= zoom;
		}
		else
		{
			this.Width /= -zoom;
		}
	}
	
	public void ZoomHeight(double zoom)
	{
		if(zoom > 0.0)
		{
			this.Height *= zoom;
		}
		else
		{
			this.Height /= -zoom;
		}
	}
	
	public void ChangeSize(double width, double height)
	{
		this.Width = width;
		this.Height = height;
	}	
	
	public void ChangeWidth(double width)
	{
		this.Width = width;
	}
	
	public void ChangeHeight(double height)
	{
		this.Height = height;
	}		
	
	public override string ToString()
	{
		return String.Format("Rectangle with top left point at ({0}, {1}), width = {2}, height = {3}", 
							this._x, this._y, this.Width, this.Height);
	}
}

class Program
{
	static void Main()
	{
		Console.WriteLine("Hello! This is my program #2!\n");
		
		Rectangle rectangle1 = new Rectangle();
		Rectangle rectangle2 = new Rectangle(4, 5);
		Rectangle rectangle3 = new Rectangle(2, 2, 3, 6);
		
		Console.WriteLine("I've just created 3 rectangles.\n");
		
		Console.WriteLine(rectangle1.ToString());
		Console.WriteLine(rectangle2.ToString());
		Console.WriteLine(rectangle3.ToString());		
		
		
		Console.WriteLine("\n\nLet's move rectangle1 through axis X on 5 units and through axis Y on 4 units.");
		rectangle1.MoveRectangle(5, 4);
		Console.WriteLine("So we have moved rectangle1: \n{0}", rectangle1.ToString());
		Console.WriteLine("\nLet's move it again! (1 unit down and 1 unit right)");
		rectangle1.MoveRectangle(1, 1);
		Console.WriteLine("{0}\n\n", rectangle1.ToString());		
		
		Console.WriteLine("Now let's change location of rectangle1.");
		rectangle1.ChangeLocation(1, 2);
		Console.WriteLine("We have changed location of rectangle1: \n{0}\n\n", rectangle1.ToString());		
		
		
		Console.WriteLine("Let's increase rectangle2!");
		rectangle2.ZoomSize(2);
		Console.WriteLine("Result: \n{0}", rectangle2.ToString());
		
		Console.WriteLine("\nNow let's decrease it!");
		rectangle2.ZoomSize(-2);
		Console.WriteLine("Result: \n{0}", rectangle2.ToString());
		
		Console.WriteLine("\nLet's increase height and decrease width!");
		rectangle2.ZoomWidth(-2);
		rectangle2.ZoomHeight(2);
		Console.WriteLine("Result: \n{0}\n\n", rectangle2.ToString());
		
		
		Console.WriteLine("Let's change size of rectangle3!");
		rectangle3.ChangeSize(5, 10);
		Console.WriteLine("Result: \n{0}", rectangle3.ToString());
		
		Console.WriteLine("\nLet's change width of rectangle3!");
		rectangle3.ChangeWidth(4);
		Console.WriteLine("Result: \n{0}", rectangle3.ToString());
		
		Console.WriteLine("\nLet's change height of rectangle3!");
		rectangle3.ChangeHeight(4);
		Console.WriteLine("Result: \n{0}\n\n", rectangle3.ToString());
		
		
		Console.WriteLine("Rectangle1 : \n{0}", rectangle1.ToString());
		Console.WriteLine("Rectangle2 : \n{0}", rectangle2.ToString());
		Console.WriteLine("Rectangle3 : \n{0}\n", rectangle3.ToString());
		
		Console.WriteLine("The least rectangle that contains rectangle1 and rectangle3 :");
		Rectangle.TheLeastRectangle(rectangle1, rectangle3);
		Console.WriteLine("\nThe least rectangle that contains rectangle2 and rectangle3 :");
		Rectangle.TheLeastRectangle(rectangle2, rectangle3);
		
		
		Rectangle rectangle4 = new Rectangle(1, 5, 7, 3); 
		Console.WriteLine("\n\nRectangle1 : \n{0}", rectangle1.ToString());
		Console.WriteLine("Rectangle2 : \n{0}", rectangle2.ToString());
		Console.WriteLine("Rectangle3 : \n{0}", rectangle3.ToString());
		Console.WriteLine("Rectangle4 : \n{0}\n", rectangle4.ToString());
		
		Console.WriteLine("Result of intersection rectangle1 and rectangle3:");
		Rectangle.ResultOfIntersection(rectangle1, rectangle3);
		
		Console.WriteLine("\nResult of intersection rectangle2 and rectangle4:");
		Rectangle.ResultOfIntersection(rectangle2, rectangle4);
		
		Console.WriteLine("\nResult of intersection rectangle3 and rectangle4:");
		Rectangle.ResultOfIntersection(rectangle3, rectangle4);
		
		Console.WriteLine("\nResult of intersection rectangle1 and rectangle4:");
		Rectangle.ResultOfIntersection(rectangle1, rectangle4);
	}
}
