using System;

public class Matrix
{
	private int[,] _matrix;
	private int _rowCount;
	private int _columnCount;
	
	public Matrix(int row, int column)
	{
		this._matrix = new int[row, column];
		this._rowCount = row;
		this._columnCount = column;
	}
	
	public Matrix(int size)
	{
		this._matrix = new int[size, size];
		this._rowCount = size;
		this._columnCount = size;		
	}
	
	public static Matrix operator+ (Matrix matrixA, Matrix matrixB)
	{
		if(!SizesAreEqual(matrixA, matrixB))
		{
			throw new FormatException("Two matrixes have different size so they can not be added!");
		}
		
		Matrix result = new Matrix(matrixA._rowCount, matrixA._columnCount);
		
		for(var row = 0; row < matrixA._rowCount; row++)
		{
			for(var column = 0; column <  matrixA._columnCount; column++)
			{
				result._matrix[row, column] = matrixA._matrix[row, column] + matrixB._matrix[row, column];
			}
		}
		
		return result;			
	}
	
	public static Matrix operator- (Matrix matrixA, Matrix matrixB)
	{
		if(!SizesAreEqual(matrixA, matrixB))
		{
			throw new FormatException("Two matrixes have different size so they can not be subtracted!");
		}
		
		Matrix result = new Matrix(matrixA._rowCount, matrixA._columnCount);
		
		for(var row = 0; row < matrixA._rowCount; row++)
		{
			for(var column = 0; column <  matrixA._columnCount; column++)
			{
				result._matrix[row, column] = matrixA._matrix[row, column] - matrixB._matrix[row, column];
			}
		}
		
		return result;		
	}
	
	public static Matrix operator* (Matrix matrixA, Matrix matrixB)
	{
		if(!SizesAreGoodForMultiplication(matrixA, matrixB))
		{
			throw new FormatException("These matrix can not be multiplied!");
		}
			
		Matrix result = new Matrix(matrixA._rowCount, matrixB._columnCount);
		
		for(var row = 0; row < result._rowCount; row++)
		{
			for(var column = 0; column <  result._columnCount; column++)
			{
				result._matrix[row, column] = 0;
				for(var inner = 0; inner < matrixA._columnCount; inner++)
				{
					result._matrix[row, column] += (matrixA._matrix[row, inner] * matrixB._matrix[inner, column]);
				}
			}
		}
		
		return result;		
	}
	
	public static Matrix operator* (Matrix matrix, int scalar)
	{
		for(var row = 0; row < matrix._rowCount; row++)
		{
			for(var column = 0; column <  matrix._columnCount; column++)
			{
				matrix._matrix[row, column] *= scalar;
			}
		}
		
		return matrix;
	}	
	
	public static Matrix Transposition(Matrix matrix)
	{
		Matrix result = new Matrix(matrix._columnCount, matrix._rowCount);
		
		for(var row = 0; row < matrix._rowCount; row++)
		{
			for(var column = 0; column <  matrix._columnCount; column++)
			{
				result._matrix[column, row] = matrix._matrix[row, column];
			}
		}
		
		return result;
	}
	
	public void Add(Matrix matrixToAdd)
	{
		if(!SizesAreEqual(this, matrixToAdd))
		{
			Console.WriteLine("Two matrixes have different size so they can not be added!");
			return;
		}		
		
		for(var row = 0; row < this._rowCount; row++)
		{
			for(var column = 0; column <  this._columnCount; column++)
			{
				this._matrix[row, column] += matrixToAdd._matrix[row, column];
			}
		}
	}
	
	public void Subtract(Matrix matrixToSubtract)
	{
		if(!SizesAreEqual(this, matrixToSubtract))
		{
			Console.WriteLine("Two matrixes have different size so they can not be subtracted!");
			return;
		}		
		
		for(var row = 0; row < this._rowCount; row++)
		{
			for(var column = 0; column <  this._columnCount; column++)
			{
				this._matrix[row, column] -= matrixToSubtract._matrix[row, column];
			}
		}
	}
	
	public void Multiply(Matrix matrixToMultiply)
	{
		if(!SizesAreGoodForMultiplication(this, matrixToMultiply))
		{
			Console.WriteLine("These matrix can not be multiplied!");
			return;
		}
			
		Matrix result = new Matrix(this._rowCount, matrixToMultiply._columnCount);
		
		for(var row = 0; row < result._rowCount; row++)
		{
			for(var column = 0; column <  result._columnCount; column++)
			{
				result._matrix[row, column] = 0;
				for(var inner = 0; inner < this._columnCount; inner++)
				{
					result._matrix[row, column] += (this._matrix[row, inner] * matrixToMultiply._matrix[inner, column]);
				}
			}
		}
		
		this._rowCount = result._rowCount;
		this._columnCount = result._columnCount;
		this._matrix = result._matrix;		
	}
	
	public void Multiply(int scalar)
	{
		for(var row = 0; row < this._rowCount; row++)
		{
			for(var column = 0; column <  this._columnCount; column++)
			{
				this._matrix[row, column] *= scalar;
			}
		}
	}
	
	public void Transposition()
	{
		int[,] temporaryMatrix = new int[this._columnCount, this._rowCount];
		
		for(var row = 0; row < this._rowCount; row++)
		{
			for(var column = 0; column <  this._columnCount; column++)
			{
				temporaryMatrix[column, row] = this._matrix[row, column];
			}
		}
		
		int valueToChange = this._rowCount;
		this._rowCount = this._columnCount;
		this._columnCount = valueToChange;
		
		this._matrix = temporaryMatrix;
	}
	
	public Matrix GetSubMatrix(int height, int width, int zeroRowIndex = 0, int zeroColumnIndex = 0)
	{
		if(height + zeroRowIndex > this._rowCount && width + zeroColumnIndex > this._columnCount)
		{
			throw new IndexOutOfRangeException("Bad indexes or size of desired matrix!");
		}
			
		Matrix result = new Matrix(height, width);
		
		for(var row = 0; row < height; row++)
		{
			for(var column = 0; column <  width; column++)
			{
				result._matrix[row, column] = this._matrix[row + zeroRowIndex, column + zeroColumnIndex];
			}
		}
		
		return result;
	}
	
	public void RandomInitializeMatrix(int minValue = 1, int maxValue = 15)
	{
		Random random = new Random();
		
		for(var row = 0; row < this._rowCount; row++)
		{
			for(var column = 0; column <  this._columnCount; column++)
			{
				this._matrix[row, column] = random.Next(minValue, maxValue);
			}
		}
	}
	
	public void ConsecutiveInitializeMatrix(int minElement = 0)
	{		
		for(var row = 0; row < this._rowCount; row++)
		{
			for(var column = 0; column <  this._columnCount; column++)
			{
				this._matrix[row, column] = ++minElement;
			}
		}
	}	
	
	public override string ToString()
	{
		System.Text.StringBuilder result = new System.Text.StringBuilder();
		
		for(var row = 0; row < this._rowCount; row++)
		{
			for(var column = 0; column <  this._columnCount; column++)
			{
				result.Append(this._matrix[row, column]);
				result.Append(" ");
			}
			result.Append("\n");
		}		
		
		return result.Append("\n").ToString();
	}
	
	public override int GetHashCode()
	{
		return this._matrix.GetHashCode();
	}
	
	public override bool Equals(object obj)
	{
		if(obj == null)
		{
			return false;
		}
		
		Matrix matrixToCompare = obj as Matrix;
		if(matrixToCompare == null)
		{
			return false;
		}
		
		if(!SizesAreEqual(this, matrixToCompare))
		{
			return false;
		}
		
		for(var row = 0; row < this._rowCount; row++)
		{
			for(var column = 0; column <  this._columnCount; column++)
			{
				if(this._matrix[row, column] != matrixToCompare._matrix[row, column])
				{
					return false;
				}
			}
		}
		
		return true;
	}
	
	private static bool SizesAreEqual(Matrix matrixA, Matrix matrixB)
    {
        if (matrixA._rowCount == matrixB._rowCount && matrixA._columnCount == matrixB._columnCount)
        {
            return true;
        }		
        return false;
    }
	
	private static bool SizesAreGoodForMultiplication(Matrix matrixA, Matrix matrixB)
	{
		if(matrixA._columnCount == matrixB._rowCount)
		{
			return true;
		}		
		return false;
	}
}

class Program
{
	static void Main()
	{
		Console.WriteLine("Hello! This is my program #4!\n");
		
		Matrix matrixA = new Matrix(4, 7);
		Matrix matrixB = new Matrix(4, 7);
		Matrix matrixC = new Matrix(5, 4);
		Matrix matrixD = new Matrix(7, 6);
		
		Console.WriteLine("I've just created 4 matrixes.");
		Console.WriteLine("Let's initialize them.");
		
		matrixA.ConsecutiveInitializeMatrix();
		matrixB.RandomInitializeMatrix(2, 7);
		matrixC.RandomInitializeMatrix(3, 10);
		matrixD.ConsecutiveInitializeMatrix(5);
		
		Console.WriteLine("\nmatrixA: \n{0}", matrixA.ToString());
		Console.WriteLine("matrixB: \n{0}", matrixB.ToString());
		Console.WriteLine("matrixC: \n{0}", matrixC.ToString());
		Console.WriteLine("matrixD: \n{0}", matrixD.ToString());
		
		Console.WriteLine("Let's add and subtract two matrixes!");
		Matrix result;
		
		try
		{
			Console.WriteLine("matrixA + matrixB :");
			result = matrixA + matrixB;
			Console.WriteLine("result: \n{0}", result.ToString());
			
			Console.WriteLine("matrixA - matrixB :");
			result = matrixA - matrixB;
			Console.WriteLine("result: \n{0}", result.ToString());
		}
		catch(FormatException exception)
		{
			Console.WriteLine(exception.Message);
		}
		
		Console.WriteLine("\nLet's add and subtract another matrixes!");
		try
		{
			Console.WriteLine("matrixA + matrixC :");
			result = matrixA + matrixC;
			Console.WriteLine("result: \n{0}", result.ToString());
			
			Console.WriteLine("matrixA - matrixC :");
			result = matrixA - matrixC;
			Console.WriteLine("result: \n{0}", result.ToString());
		}
		catch(FormatException exception)
		{
			Console.WriteLine(exception.Message);
			Console.WriteLine("Ups! Something wrong :)");
		}
		
		Console.WriteLine("\nLet's multiply two matrixes!");
		try
		{
			Console.WriteLine("matrixC * matrixB :");
			result = matrixC * matrixB;
			Console.WriteLine("result: \n{0}", result.ToString());			
		}
		catch(FormatException exception)
		{
			Console.WriteLine(exception.Message);			
		}
		
		Console.WriteLine("\nLet's multiply another matrixes!");
		try
		{
			Console.WriteLine("matrixA * matrixB :");
			result = matrixA * matrixB;
			Console.WriteLine("result: \n{0}", result.ToString());			
		}
		catch(FormatException exception)
		{
			Console.WriteLine(exception.Message);	
			Console.WriteLine("Ups! Something wrong :)");
		}
		
		Console.WriteLine("\nLet's multiply matrixC by 2!");
		matrixC *= 2;
		Console.WriteLine("matrixC: \n{0}", matrixC.ToString());	

		Console.WriteLine("\nLet's get matrix transposed to matrixA!");
		result = Matrix.Transposition(matrixA);
		Console.WriteLine("result: \n{0}", result.ToString());
		
		
		Console.WriteLine("\nLet's change matrixes with non-static methods!");
		Console.WriteLine("\nmatrixA = matrixA + matrixB :");
		matrixA.Add(matrixB);
		Console.WriteLine(matrixA.ToString());
		
		Console.WriteLine("\nmatrixB = matrixB - matrixA :");
		matrixB.Subtract(matrixA);
		Console.WriteLine(matrixB.ToString());
		
		Console.WriteLine("\nmatrixB = matrixB + matrixD");
		matrixB.Add(matrixD);
		
		Console.WriteLine("\nmatrixC = matrixC * matrixB :");
		matrixC.Multiply(matrixB);
		Console.WriteLine(matrixC.ToString());
		
		Console.WriteLine("\nmatrixA = matrixA * matrixB :");
		matrixA.Multiply(matrixB);		
		
		Console.WriteLine("\nmatrixA = matrixA * 2 :");
		matrixA.Multiply(2);
		Console.WriteLine(matrixA.ToString());
		
		Console.WriteLine("\nTransposed matrix to matrixD:");
		matrixD.Transposition();
		Console.WriteLine(matrixD.ToString());
		
		matrixD.ConsecutiveInitializeMatrix();
		Console.WriteLine("\nLet's get submatrix from matrixD!");
		Console.WriteLine("MatrixD: \n{0}", matrixD.ToString());
		Console.WriteLine("Submatrix:");
		try
		{
			result = matrixD.GetSubMatrix(3, 5, 2, 2);
			Console.WriteLine(result.ToString());
		}
		catch(IndexOutOfRangeException exception)
		{
			Console.WriteLine(exception.Message);
		}
		
		Console.WriteLine("Another submatrix:");
		try
		{
			result = matrixD.GetSubMatrix(2, 4);
			Console.WriteLine(result.ToString());
		}
		catch(IndexOutOfRangeException exception)
		{
			Console.WriteLine(exception.Message);
		}
		
		Console.WriteLine("Another submatrix:");
		try
		{
			result = matrixD.GetSubMatrix(10, 5, 2, 2);
			Console.WriteLine(result.ToString());
		}
		catch(IndexOutOfRangeException exception)
		{
			Console.WriteLine(exception.Message);
			Console.WriteLine("Ups! Something wrong :)");
		}
		
		matrixA.ConsecutiveInitializeMatrix();
		matrixB.ConsecutiveInitializeMatrix();
		
		Console.WriteLine("\nmatrixA: \n{0}", matrixA.ToString());
		Console.WriteLine("\nmatrixB: \n{0}", matrixB.ToString());
		Console.WriteLine("\nmatrixC: \n{0}", matrixC.ToString());
		
		Console.WriteLine("matrixA == matrixB : {0}", matrixA.Equals(matrixB));
		Console.WriteLine("matrixA == matrixC : {0}", matrixA.Equals(matrixC));	
	}
}